// import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){

	// console.log(props)

	// useState hook:
	// a hook in React is a kind of tool.
	// array destructuring to get the state and the setter. 
	// The useState hook allows creation and manipulation of states.
	//States are a way for React to keep track of any value and associate it with a component
	// When a state changes, React re-renders ONLY the specific component or part of the component that changes (and not the entire page or unrelated components)
	//state: a special kind of variable (can be named anything) that React uses to render/re-render components when needed

	//state setter: State setters are the ONLY way to change a state's value

	//default state: The state's initial value on component mount

	//Before a component mounts, a state actually defaults to undefined, THEN is changed to its default state

	// const [count, setCount] = useState(0)
	// const [seats, setSeats] = useState(10)
	// const [name, setName] = useState(0)


	//syntax:

	// const [state, setState] = useState(default state)


	let { name, description, price, id } = courseProp;


	// refactor the enroll function and instead use "useEffect" hooks
	// function enroll(){
	// 	if (count !== 10) {
	// 		setCount(count + 1);
	// 		setSeats(seats - 1);
	// 	}
	// }

	//Apply the use Effect Hook

	//useEffect makes any given code block happen when a state changes AND when a component first mounts (such as on initial page load)


	// Syntax:

		//useEffect(function,[dependencies])




	// useEffect(() => {
	// 	if(seats === 0){
	// 		alert("No more seats available!")
	// 	}
	// }, [count, seats])

	
	// alert("No more slots available.")
	/*
		ACTIVITY:
		Create a state hook that will represent the number of available seats for each course. 

		It should default to 10 and decrement by 1 each time a student enrolls.

		Add a condition that will show an alert that no more seats are available if the seats state is 0. 


	*/
	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				<Card.Text>Enrollees: </Card.Text>
				<Card.Text>Slots Available: </Card.Text>
				<Button as={Link} to = {`/courses/${id}`}>Details</Button>
			</Card.Body>
		</Card>
	)
}